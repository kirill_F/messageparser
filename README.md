# Android Take Home Test #

Android application that takes a chat message string and returns a JSON string containing information about its contents. App has single activity MessageParserActivity with EditText for message input, RecyclerView for representing proccessed messages.

Adapter item has 2 modes of representation - rendered and raw. Each item has mode identifier under the message text. Click on identifier to toggle representation mode.

Rendered mode applies styling on special content like highlighting links, colouring mentions and emoticons. 
Raw mode represents json with information about message content.

## Implementation details

App is implemented in Kotlin with Clean Architecture approach. There are 3 layers - data, domain, presentation (ui). 

* Data layer consists of message repository implementataion. Simple in-memory collection. Repository is an interface so implementation can be extended with remote storage, persistance storage and so on without affecting other parts of application.  
* Domain layer consists of domain model and interactor (usecase in other words). Often usecase is implemented with Command design pattern, i have another approach that is defining interactor for a feature, it can be implemented as a single class with business login or with Facade design pattern that contains another usecases. Interactor contains all logic for parsing string and representing it as domain Message object. Parser is a FSM (finite-state machine) with several states for different content types. It takes O(n) time and 2N memory to parse message string. 
* Representation layer is implemeted with MVP. It consists of model (that is not a ViewModel from MVVM) - UI representation of domain Message object it is ready for drawing in UI. Presenter that contains all representation logic and maps and holds view models. Presenter handles all events from UI. View interface for showing content to user. Activite is implementing View interface.
* All components are created in activity. It is possible to use DI (for example with Dagger2), create complonents in activity scope and inject implementations. Presenter is retained with onRetainCustomNonConfigurationInstance() it is simple solution for retainig objects. It can be switchedto Mosby/Moxy/etc. 
* Json rendering is made manually. It is possible to use Gson with annotations, but for that task i need custom json serialization so i need to implementcustom type adapters. Looks like overengineering for the task purposes.
* Scheduling is made with simple Executor interface. Implementation can be extended. Results of async operations are delivered with simple callbacks. Can be replace by RxJava or Kotlin coroutines.
* Most of tests are plain JUnit tests that can be run on JVM. Most of context depenedent objects and components are hidden behind interfaces.