package com.github.kirillf.messageparser.interactor

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * Created by k.filimonov on 16/08/2017.
 */
class UrlValidatorImplTest {
    private lateinit var urlValidator: UrlValidator

    @Before
    fun setUp() {
        urlValidator = UrlValidatorImpl()
    }

    @Test
    fun shouldReturnValidWhenCorrectUrlWithoutParams() {
        val url = "https://atlassian.com"
        assertTrue(urlValidator.isUrlValid(url))
    }

    @Test
    fun shouldReturnValidWhenCorrectWithWithPath() {
        val url = "https://atlassian.com/careers"
        assertTrue(urlValidator.isUrlValid(url))
    }

    @Test
    fun shouldReturnValidWhenCorrectUrlWithParams() {
        val url = "https://atlassian.com/careers/?param=1&params=2"
        assertTrue(urlValidator.isUrlValid(url))
    }

    @Test
    fun shouldReturnInvalidWhenProtocolNotSpecified() {
        val url = "atlassian.com/careers/?param=1&params=2"
        assertFalse(urlValidator.isUrlValid(url))
    }

    @Test
    fun shouldReturnInvalidWhenProtocolIncorrect() {
        val url = "hmms://atlassian.com/careers/?param=1&params=2"
        assertFalse(urlValidator.isUrlValid(url))
    }

    @Test
    fun shouldReturnInvalidWhenInvalidSymbolsHost() {
        val url = "https://atl assian.com/careers/&param?param&=1"
        assertFalse(urlValidator.isUrlValid(url))
    }

    @Test
    fun shouldReturnInvalidWhenInvalidSymbolsInParams() {
        val url = "https://atlassian.com/careers/?param 2"
        assertFalse(urlValidator.isUrlValid(url))
    }

    @Test
    fun shouldParseHttpScheme() {
        val url = "http://atlassian.com/careers/?param"
        assertTrue(urlValidator.isUrlValid(url))
    }
}
