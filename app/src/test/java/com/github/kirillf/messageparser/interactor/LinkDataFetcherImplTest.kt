package com.github.kirillf.messageparser.interactor

import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * Created by k.filimonov on 16/08/2017.
 */
class LinkDataFetcherImplTest {
    val testTitle = "testTitle"
    val htmlWithTitle = "<head><title>$testTitle</title></head><body/>"

    private lateinit var dataFetcher: LinkDataFetcher

    lateinit var webServer: MockWebServer

    @Before
    fun setUp() {
        dataFetcher = LinkDataFetcherImpl()
        webServer = MockWebServer()
        webServer.start()
    }

    @After
    fun tearDown() {
        webServer.shutdown()
    }

    @Test
    fun shouldReturnTitleWhenOkResponseAndHtmlContainsTitle() {
        webServer.enqueue(MockResponse().setResponseCode(200).setBody(htmlWithTitle))

        val actual = dataFetcher.fetchTitle(webServer.url("").toString())
        assertEquals(testTitle, actual)
    }

    @Test
    fun shouldReturnEmptyTitleWhenNonHtmlResponse() {
        webServer.enqueue(MockResponse().setResponseCode(200).setBody("{}"))

        val actual = dataFetcher.fetchTitle(webServer.url("").toString())
        assertEquals("", actual)
    }

    @Test
    fun shouldReturnEmptyTitleWhenInternalServerError() {
        webServer.enqueue(MockResponse().setResponseCode(500))

        val actual = dataFetcher.fetchTitle(webServer.url("").toString())
        assertEquals("", actual)
    }

    @Test
    fun shouldReturnEmptyTitleWhenNotFound() {
        webServer.enqueue(MockResponse().setResponseCode(404).setBody("<head><title>Not found</title></head><body/>"))

        val actual = dataFetcher.fetchTitle(webServer.url("").toString())
        assertEquals("", actual)
    }
}