package com.github.kirillf.messageparser.interactor

import com.github.kirillf.messageparser.model.Emoticon
import com.github.kirillf.messageparser.model.Link
import com.github.kirillf.messageparser.model.Mention
import com.github.kirillf.messageparser.model.Plain
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.Mockito.*

/**
 * Created by k.filimonov on 13/08/2017.
 */
@RunWith(MockitoJUnitRunner::class)
class MessageParserImplTest {

    @Mock
    private lateinit var urlValidator : UrlValidator

    @Mock
    private lateinit var urlFetcher : LinkDataFetcher

    private lateinit var parser: MessageParserImpl

    @Before
    fun setUp() {
        parser = MessageParserImpl(urlFetcher, urlValidator)
        `when`(urlFetcher.fetchTitle(ArgumentMatchers.anyString())).thenReturn("")
    }

    @Test
    fun shouldParseEmptyMessageWhenEmptyInputString() {
        val message = parser.parse("")
        assertTrue(message.tokens.isEmpty())
        assertTrue(message.emoticon.isEmpty())
        assertTrue(message.mentions.isEmpty())
        assertTrue(message.links.isEmpty())
    }

    @Test
    fun shouldParseMessageWithSinglePlainSymbolWhenInputWithSingleDelimiter() {
        val message = parser.parse(",")
        assertTrue(message.emoticon.isEmpty())
        assertTrue(message.mentions.isEmpty())
        assertTrue(message.links.isEmpty())
        assertEquals(1, message.tokens.size)
        assertEquals(Plain(","), message.tokens[0])
    }

    @Test
    fun shouldParseMultipleDelimitersWhenMessageInputIsMultipleDelimiters() {
        val message = parser.parse(",. ,.\n, ")
        assertTrue(message.emoticon.isEmpty())
        assertTrue(message.mentions.isEmpty())
        assertTrue(message.links.isEmpty())
        assertEquals(8, message.tokens.size)
        assertEquals(listOf(Plain(","), Plain("."), Plain(" "), Plain(","), Plain("."),
                Plain("\n"), Plain(","), Plain(" ")), message.tokens)
    }

    @Test
    fun shouldParsePlainTextWhenMessageContainsOnlyPlainText() {
        val message = parser.parse("just plain text, no specials")
        assertTrue(message.emoticon.isEmpty())
        assertTrue(message.mentions.isEmpty())
        assertTrue(message.links.isEmpty())
        assertEquals(10, message.tokens.size)
        assertEquals(listOf(Plain("just"), Plain(" "), Plain("plain"), Plain(" "), Plain("text"),
                Plain(","), Plain(" "), Plain("no"), Plain(" "), Plain("specials")), message.tokens)
    }

    @Test
    fun shouldParseSingleMentionWhenOnlyMentionInMessage() {
        val message = parser.parse("@joe")
        assertTrue(message.links.isEmpty())
        assertTrue(message.emoticon.isEmpty())

        assertEquals(1, message.tokens.size)
        assertEquals(Mention("joe"), message.tokens[0])

        assertEquals(1, message.mentions.size)
        assertEquals(Mention("joe"), message.mentions[0])
    }

    @Test
    fun shouldParseMultipleMentions() {
        val message = parser.parse("@joe @chris, @man")
        assertTrue(message.links.isEmpty())
        assertTrue(message.emoticon.isEmpty())

        assertEquals(3, message.mentions.size)
        assertEquals(listOf(Mention("joe"), Mention("chris"), Mention("man")), message.mentions)

        assertEquals(6, message.tokens.size)
        assertEquals(listOf(Mention("joe"), Plain(" "), Mention("chris"), Plain(","), Plain(" "), Mention("man")), message.tokens)
    }

    @Test
    fun shouldNotParseIncorrectMention() {
        val message = parser.parse("@joe%*")
        assertTrue(message.links.isEmpty())
        assertTrue(message.emoticon.isEmpty())
        assertTrue(message.mentions.isEmpty())

        assertEquals(1, message.tokens.size)
        assertEquals(listOf(Plain("@joe%*")), message.tokens)
    }

    @Test
    fun shouldNotParseMentionsWithoutDelimiter() {
        val message = parser.parse("@joe@chris")
        assertTrue(message.links.isEmpty())
        assertTrue(message.emoticon.isEmpty())
        assertTrue(message.mentions.isEmpty())

        assertEquals(1, message.tokens.size)
        assertEquals(listOf(Plain("@joe@chris")), message.tokens)
    }

    @Test
    fun shouldNotParseMentionInPlainTextWithoutDelimiter() {
        val message = parser.parse("joe@chris")
        assertTrue(message.links.isEmpty())
        assertTrue(message.emoticon.isEmpty())
        assertTrue(message.mentions.isEmpty())

        assertEquals(1, message.tokens.size)
        assertEquals(listOf(Plain("joe@chris")), message.tokens)
    }

    @Test
    fun shouldParseMentionInPlainTextRightAfterDelimiter() {
        val message = parser.parse("joe,@chris")
        assertTrue(message.links.isEmpty())
        assertTrue(message.emoticon.isEmpty())

        assertEquals(1, message.mentions.size)
        assertEquals(listOf(Mention("chris")), message.mentions)

        assertEquals(3, message.tokens.size)
        assertEquals(listOf(Plain("joe"), Plain(","), Mention("chris")), message.tokens)
    }

    @Test
    fun shouldParseMentionRightBeforeDelimiter() {
        val message = parser.parse("@chris.")
        assertTrue(message.links.isEmpty())
        assertTrue(message.emoticon.isEmpty())

        assertEquals(1, message.mentions.size)
        assertEquals(listOf(Mention("chris")), message.mentions)

        assertEquals(2, message.tokens.size)
        assertEquals(listOf(Mention("chris"), Plain(".")), message.tokens)
    }

    @Test
    fun shouldParsePlainTextWhenIncorrectMentionRightBeforeDelimiter() {
        val message = parser.parse("@.")
        assertTrue(message.links.isEmpty())
        assertTrue(message.emoticon.isEmpty())
        assertTrue(message.mentions.isEmpty())

        assertEquals(2, message.tokens.size)
        assertEquals(listOf(Plain("@"), Plain(".")), message.tokens)
    }

    @Test
    fun shouldParseMessageFromControlExample() {
        val message = parser.parse("@chris you around?")
        assertTrue(message.links.isEmpty())
        assertTrue(message.emoticon.isEmpty())

        assertEquals(1, message.mentions.size)
        assertEquals(Mention("chris"), message.mentions[0])

        assertEquals(5, message.tokens.size)
        assertEquals(listOf(Mention("chris"), Plain(" "), Plain("you"), Plain(" "), Plain("around?")), message.tokens)
    }

    @Test
    fun shouldParseSimpleEmoticon() {
        val message = parser.parse("(coffee)")
        assertTrue(message.links.isEmpty())
        assertTrue(message.mentions.isEmpty())

        assertEquals(1, message.emoticon.size)
        assertEquals(Emoticon("coffee"), message.emoticon[0])

        assertEquals(1, message.tokens.size)
        assertEquals(listOf(Emoticon("coffee")), message.tokens)
    }

    @Test
    fun shouldNotParseEmoticonAsPlainWhenEmoticonLargerThan15Symbols() {
        val message = parser.parse("(coffeecoffeecoffee)")
        assertTrue(message.links.isEmpty())
        assertTrue(message.mentions.isEmpty())
        assertTrue(message.emoticon.isEmpty())

        assertEquals(1, message.tokens.size)
        assertEquals(listOf(Plain("(coffeecoffeecoffee)")), message.tokens)
    }

    @Test
    fun shouldNotParseEmoticonWhenNoClosingBrace() {
        val message = parser.parse("(coffee")
        assertTrue(message.links.isEmpty())
        assertTrue(message.mentions.isEmpty())
        assertTrue(message.emoticon.isEmpty())

        assertEquals(1, message.tokens.size)
        assertEquals(listOf(Plain("(coffee")), message.tokens)
    }

    @Test
    fun shouldParseMessageFromControlTest2() {
        val message = parser.parse("Good morning! (megusta) (coffee)")
        assertTrue(message.links.isEmpty())
        assertTrue(message.mentions.isEmpty())

        assertEquals(2, message.emoticon.size)
        assertEquals(listOf(Emoticon("megusta"), Emoticon("coffee")), message.emoticon)

        assertEquals(7, message.tokens.size)
        assertEquals(listOf(Plain("Good"), Plain(" "), Plain("morning!"),
                Plain(" "), Emoticon("megusta"), Plain(" "), Emoticon("coffee")), message.tokens)
    }

    @Test
    fun shouldParseEmoticonFromTheMiddleOfText() {
        val message = parser.parse("Good(coffee)morning!(megusta)")
        assertTrue(message.links.isEmpty())
        assertTrue(message.mentions.isEmpty())

        assertEquals(2, message.emoticon.size)
        assertEquals(listOf(Emoticon("coffee"), Emoticon("megusta")), message.emoticon)

        assertEquals(4, message.tokens.size)
        assertEquals(listOf(Plain("Good"), Emoticon("coffee"), Plain("morning!"),
                Emoticon("megusta")), message.tokens)
    }

    @Test
    fun shouldParseLinkWhenOnlyValidLinkInMessage() {
        `when`(urlValidator.isUrlValid(ArgumentMatchers.anyString())).thenReturn(true)
        val message = parser.parse("http://link.com")
        assertTrue(message.emoticon.isEmpty())
        assertTrue(message.mentions.isEmpty())

        assertEquals(1, message.links.size)
        assertEquals(listOf(Link("http://link.com")), message.links)

        assertEquals(1, message.tokens.size)
        assertEquals(listOf(Link("http://link.com")), message.tokens)
    }

    @Test
    fun shouldParseMessageFromControl3() {
        `when`(urlValidator.isUrlValid(ArgumentMatchers.anyString())).thenReturn(true)
        val message = parser.parse("Olympics are starting soon; http://www.nbcolympics.com")
        assertTrue(message.emoticon.isEmpty())
        assertTrue(message.mentions.isEmpty())

        assertEquals(1, message.links.size)
        assertEquals(listOf(Link("http://www.nbcolympics.com")), message.links)

        assertEquals(9, message.tokens.size)
        assertEquals(listOf(Plain("Olympics"), Plain(" "), Plain("are"), Plain(" "), Plain("starting"),
                Plain(" "), Plain("soon;"), Plain(" "), Link("http://www.nbcolympics.com")), message.tokens)
    }

    @Test
    fun shouldParseMessageFromControl4() {
        `when`(urlValidator.isUrlValid(ArgumentMatchers.anyString())).thenReturn(true)
        val message = parser.parse("@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016")
        assertEquals(1, message.emoticon.size)
        assertEquals(listOf(Emoticon("success")), message.emoticon)

        assertEquals(2, message.mentions.size)
        assertEquals(listOf(Mention("bob"), Mention("john")), message.mentions)

        assertEquals(1, message.links.size)
        assertEquals(listOf(Link("https://twitter.com/jdorfman/status/430511497475670016")), message.links)

        assertEquals(15, message.tokens.size)
        assertEquals(listOf(Mention("bob"), Plain(" "), Mention("john"), Plain(" "),
                Emoticon("success"), Plain(" "), Plain("such"), Plain(" "), Plain("a"),
                Plain(" "), Plain("cool"), Plain(" "), Plain("feature;"), Plain(" "),
                Link("https://twitter.com/jdorfman/status/430511497475670016")), message.tokens)
    }

    @Test
    fun shouldParseUrlAsPlainWhenUrlIsInvalid() {
        `when`(urlValidator.isUrlValid(ArgumentMatchers.anyString())).thenReturn(false)
        val message = parser.parse("http://link.com")
        assertTrue(message.emoticon.isEmpty())
        assertTrue(message.mentions.isEmpty())
        assertTrue(message.links.isEmpty())


        assertEquals(1, message.tokens.size)
        assertEquals(listOf(Plain("http://link.com")), message.tokens)
    }

}