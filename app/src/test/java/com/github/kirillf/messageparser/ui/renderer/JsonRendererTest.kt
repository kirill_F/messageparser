package com.github.kirillf.messageparser.ui.renderer

import com.github.kirillf.messageparser.model.Emoticon
import com.github.kirillf.messageparser.model.Link
import com.github.kirillf.messageparser.model.Mention
import com.github.kirillf.messageparser.model.Message
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * Created by k.filimonov on 16/08/2017.
 */
class JsonRendererTest {

    private lateinit var renderer: Renderer<String>

    @Before
    fun setUp() {
        renderer = JsonRenderer()
    }

    @Test
    fun shouldRenderEmptyJsonWhenMessageWithNoSpecial() {
        val message = renderer.render(Message(listOf()))
        assertEquals("{}", message)
    }

    @Test
    fun shouldRenderJsonWithOnlyMentionsWhenMessageContainsOnlyMentions() {
        val mention1 = Mention("mike")
        val mention2 = Mention("joe")
        val message = renderer.render(Message(listOf(), listOf(mention1, mention2)))
        assertEquals("{\"mentions\":[\"mike\",\"joe\"]}", message)
    }

    @Test
    fun shouldRenderJsonWithOnlyEmoticonWhenMessageContainsOnlyEmoticons() {
        val emoticon1 = Emoticon("coffee")
        val emoticon2 = Emoticon("megusta")
        val message = renderer.render(Message(listOf(), emoticon = listOf(emoticon1, emoticon2)))
        assertEquals("{\"emoticons\":[\"coffee\",\"megusta\"]}", message)
    }

    @Test
    fun shouldRenderJsonWithOnlyLinksWhenMessageContainsOnlyLinks() {
        val link1 = Link("http://example.com", "example url")
        val link2 = Link("http://example.co.uk")
        val message = renderer.render(Message(listOf(), links = listOf(link1, link2)))
        assertEquals("{\"links\":[{\"url\":\"http://example.com\",\"title\":\"example url\"},{\"url\":\"http://example.co.uk\"}]}", message)
    }

    @Test
    fun shouldRenderControlExample1() {
        val message = renderer.render(Message(listOf(), mentions = listOf(Mention("chris"))))
        assertEquals("{\"mentions\":[\"chris\"]}", message)
    }

    @Test
    fun shouldRenderControlExample2() {
        val message = renderer.render(Message(listOf(), emoticon = listOf(Emoticon("coffee"), Emoticon("megusta"))))
        assertEquals("{\"emoticons\":[\"coffee\",\"megusta\"]}", message)
    }

    @Test
    fun shouldRenderControlExample3() {
        val message = renderer.render(Message(listOf(), links = listOf(Link("http://www.nbcolympics.com", "NBC Olympics | 2014 NBC Olympics in Sochi Russia"))))
        assertEquals("{\"links\":[{\"url\":\"http://www.nbcolympics.com\",\"title\":\"NBC Olympics | 2014 NBC Olympics in Sochi Russia\"}]}", message)
    }

    @Test
    fun shouldRenderControlExample4() {
        val message = renderer.render(Message(listOf(),
                links = listOf(Link("https://twitter.com/jdorfman/status/430511497475670016", "Twitter / jdorfman: nice @littlebigdetail from ...")),
                mentions = listOf(Mention("bob"), Mention("john")),
                emoticon = listOf(Emoticon("success"))))
        assertEquals("{\"mentions\":[\"bob\",\"john\"],\"emoticons\":[\"success\"],\"links\":[{\"url\":\"https://twitter.com/jdorfman/status/430511497475670016\",\"title\":\"Twitter / jdorfman: nice @littlebigdetail from ...\"}]}", message)
    }

}