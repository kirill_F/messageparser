package com.github.kirillf.messageparser.ui

import android.text.SpannableString
import com.github.kirillf.messageparser.ImmediateExecutor
import com.github.kirillf.messageparser.interactor.MessagesInteractor
import com.github.kirillf.messageparser.model.Message
import com.github.kirillf.messageparser.ui.model.MessageMapper
import com.github.kirillf.messageparser.ui.model.MessageViewModel
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.Mockito.*

/**
 * Created by k.filimonov on 15/08/2017.
 */
@RunWith(MockitoJUnitRunner::class)
class MessageParserPresenterImplTest {

    @Mock
    private lateinit var interactor: MessagesInteractor

    @Mock
    private lateinit var mapper: MessageMapper

    @Mock
    private lateinit var view: MessageParserPresenter.View

    private lateinit var presenter: MessageParserPresenterImpl

    @Before
    fun setUp() {
        presenter =  MessageParserPresenterImpl(interactor, mapper, ImmediateExecutor(),
                ImmediateExecutor())
    }

    @Test
    fun shouldRequestMessagesWhenViewAttached() {
        presenter.onViewAttached(view)
        verify(interactor).getMessages(presenter)
    }

    @Test
    fun shouldNotRequestUpdateViewIfNoMessagesInPresenter() {
        presenter.onViewAttached(view)
        verifyZeroInteractions(view)
    }

    @Test
    fun shouldUpdateViewOnAttachWhenOnMessageCalledWithMessages() {
        val viewMessage = MessageViewModel(MessageViewModel.ViewState.RAW, "", SpannableString(""))
        val messages = listOf(Message(listOf()))
        val viewMessages = listOf(viewMessage)

        presenter.onViewAttached(view)
        `when`(mapper.map(messages, MessageViewModel.ViewState.RENDERED)).thenReturn(viewMessages)
        presenter.onMessages(messages)

        verify(view).setMessages(viewMessages)
    }

    @Test
    fun shouldUpdateViewOnAttachIfMessagesInPresenter() {
        val messages = listOf(MessageViewModel(MessageViewModel.ViewState.RENDERED, "", SpannableString("")))
        preparePresenter(messages)

        presenter.onViewAttached(view)

        verify(view).setMessages(messages)
    }

    @Test
    fun shouldShowErrorWhenInteractorCompletedWithError() {
        presenter.onViewAttached(view)

        presenter.onError(Throwable())

        verify(view).showError()
    }

    @Test
    fun shouldUpdateMessagesOnViewWhenNewMessageParsed() {
        val message = Message(listOf())
        val viewMessage = MessageViewModel(MessageViewModel.ViewState.RENDERED, "", SpannableString(""))

        presenter.onViewAttached(view)
        `when`(mapper.map(message, MessageViewModel.ViewState.RAW)).thenReturn(viewMessage)
        presenter.onMessageParsed(message)

        verify(view).addMessage(0)
    }

    @Test
    fun shouldUpdateCorrentMessageWhenMessageAddedToExistingList() {
        preparePresenter(listOf(
                MessageViewModel(MessageViewModel.ViewState.RENDERED, "", SpannableString("")),
                MessageViewModel(MessageViewModel.ViewState.RENDERED, "", SpannableString(""))
        ))

        val message = Message(listOf())
        val viewMessage = MessageViewModel(MessageViewModel.ViewState.RENDERED, "", SpannableString(""))

        presenter.onViewAttached(view)
        `when`(mapper.map(message, MessageViewModel.ViewState.RAW)).thenReturn(viewMessage)
        presenter.onMessageParsed(message)

        verify(view).addMessage(2)
    }

    @Test
    fun shouldNotUpdateViewWhenIncorrectUpdateIndex() {
        presenter.onToggleMessage(0)
        presenter.onToggleMessage(100)

        verifyZeroInteractions(view)
    }

    @Test
    fun shouldUpdateSelectedMessage() {
        preparePresenter(listOf(MessageViewModel(MessageViewModel.ViewState.RAW, "", SpannableString(""))))
        presenter.onViewAttached(view)

        presenter.onToggleMessage(0)

        verify(view).updateMessage(0)
    }

    private fun preparePresenter(viewMessages: List<MessageViewModel>) {
        val rawMessages = listOf(Message(listOf()))

        `when`(mapper.map(rawMessages, MessageViewModel.ViewState.RENDERED)).thenReturn(viewMessages)
        presenter.onMessages(rawMessages)
    }
}