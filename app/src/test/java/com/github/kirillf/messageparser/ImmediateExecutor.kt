package com.github.kirillf.messageparser

import com.github.kirillf.messageparser.executor.Executor

/**
 * Created by k.filimonov on 15/08/2017.
 */
class ImmediateExecutor : Executor {

    override fun execute(runnable: Runnable) {
        runnable.run()
    }

}