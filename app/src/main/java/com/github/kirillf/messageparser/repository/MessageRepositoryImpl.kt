package com.github.kirillf.messageparser.repository

import com.github.kirillf.messageparser.model.*

/**
 * Created by k.filimonov on 06/08/2017.
 */
class MessageRepositoryImpl() : MessageRepository {

    private val messages = mutableListOf<Message>()
    private val lock = Object()

    override fun putMessage(message: Message) {
        synchronized(lock) {
            messages.add(message)
        }
    }

    override fun getMessages(): List<Message> {
        synchronized(lock) {
            return messages.toList()
        }
    }
}