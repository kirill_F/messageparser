package com.github.kirillf.messageparser.executor

import java.util.concurrent.Executors

/**
 * Created by k.filimonov on 06/08/2017.
 */
class BackgroundExecutor : Executor {

    private object Holder {
        val INSTANCE = BackgroundExecutor()
    }

    companion object {
        val instance: BackgroundExecutor by lazy { Holder.INSTANCE }
    }

    private val executorService = Executors.newCachedThreadPool()

    override fun execute(runnable: Runnable) {
        executorService.submit(runnable)
    }
}