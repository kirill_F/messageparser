package com.github.kirillf.messageparser.ui.renderer

import com.github.kirillf.messageparser.model.Message
import com.google.gson.*

/**
 * Created by k.filimonov on 13/08/2017.
 */

class JsonRenderer() : Renderer<String> {
    private val emoticons_field = "emoticons"
    private val links_field = "links"
    private val mentions_field = "mentions"

    private val url_property = "url"
    private val title_property = "title"

    override fun render(message: Message): String {
        val jsonMessage = JsonObject()
        if (message.mentions.isNotEmpty()) {
            jsonMessage.add(mentions_field, getJsonElement(message.mentions, { (name) -> name }))
        }
        if (message.emoticon.isNotEmpty()) {
            jsonMessage.add(emoticons_field, getJsonElement(message.emoticon, { (type) -> type }))
        }
        if (message.links.isNotEmpty()) {
            jsonMessage.add(links_field, getJsonElementWithObjects(message.links, {(link, title) -> createLinkObject(link, title)}))
        }
        return jsonMessage.toString()
    }

    private fun <M> getJsonElement(models: List<M>, convert: (M) -> String): JsonElement {
        val array = JsonArray()
        for (model in models) {
            array.add(convert(model))
        }
        return array
    }

    private fun <M> getJsonElementWithObjects(models: List<M>, convert: (M) -> JsonObject): JsonElement {
        val array = JsonArray()
        for (model in models) {
            array.add(convert(model))
        }
        return array
    }

    private fun createLinkObject(url: String, title: String): JsonObject {
        val jsonLink = JsonObject()
        jsonLink.addProperty(url_property, url)
        if (title.isNotBlank()) {
            jsonLink.addProperty(title_property, title)
        }
        return jsonLink
    }
}
