package com.github.kirillf.messageparser.interactor

import okhttp3.HttpUrl
import java.net.MalformedURLException
import java.net.URI

class UrlValidatorImpl : UrlValidator {
    override fun isUrlValid(url: String): Boolean {
        try {
            val uri = URI.create(url)
            return HttpUrl.get(uri)?.let { return true } ?: false
        } catch (e: IllegalArgumentException) {
            return false
        } catch (e: MalformedURLException) {
            return false
        }
    }
}