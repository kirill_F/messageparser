package com.github.kirillf.messageparser.ui.model

import android.text.Spannable
import com.github.kirillf.messageparser.model.Message
import com.github.kirillf.messageparser.ui.renderer.Renderer

/**
 * Created by k.filimonov on 13/08/2017.
 */
class MessageMapper(val jsonRenderer: Renderer<String>, val spanRenderer: Renderer<Spannable>) {

    fun map(message: Message, state: MessageViewModel.ViewState = MessageViewModel.ViewState.RENDERED): MessageViewModel {
        val json = jsonRenderer.render(message)
        val rendered = spanRenderer.render(message)
        return MessageViewModel(state, json, rendered)
    }

    fun map(messages: List<Message>, state: MessageViewModel.ViewState = MessageViewModel.ViewState.RENDERED): List<MessageViewModel> {
        val mapped = mutableListOf<MessageViewModel>()
        messages.forEach {
            mapped.add(map(it, state))
        }
        return mapped.toList()
    }

}