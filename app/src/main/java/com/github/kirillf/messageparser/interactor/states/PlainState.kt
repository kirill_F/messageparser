package com.github.kirillf.messageparser.interactor.states

import com.github.kirillf.messageparser.interactor.ParserStateHandler
import com.github.kirillf.messageparser.interactor.State
import com.github.kirillf.messageparser.interactor.States
import com.github.kirillf.messageparser.model.Plain

/**
 * Created by k.filimonov on 13/08/2017.
 */
class PlainState : State {

    override fun apply(text: String, nextSymbol: Char, messageParser: ParserStateHandler) {
        when {
            messageParser.isDelimiter(nextSymbol) -> {
                messageParser.addPlain(Plain(text))
                messageParser.nextState(States.DELIMITER)
            }
            messageParser.isEmoticonStart(nextSymbol) -> {
                messageParser.addPlain(Plain(text))
                messageParser.nextState(States.EMOTICON)
            }
            messageParser.isTerminal(nextSymbol) -> {
                messageParser.addPlain(Plain(text))
                messageParser.nextState(States.IDLE)
            }
        }
    }
}