package com.github.kirillf.messageparser.interactor

interface State {
    fun apply(text: String, nextSymbol: Char, messageParser: ParserStateHandler);
}