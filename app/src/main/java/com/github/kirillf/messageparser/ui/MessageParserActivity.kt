package com.github.kirillf.messageparser.ui

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager

import com.github.kirillf.messageparser.R
import com.github.kirillf.messageparser.repository.MessageRepositoryImpl
import com.github.kirillf.messageparser.interactor.LinkDataFetcherImpl
import com.github.kirillf.messageparser.interactor.MessageParserImpl
import com.github.kirillf.messageparser.interactor.MessagesInteractor
import com.github.kirillf.messageparser.interactor.UrlValidatorImpl
import com.github.kirillf.messageparser.ui.model.MessageMapper
import com.github.kirillf.messageparser.ui.model.MessageViewModel
import com.github.kirillf.messageparser.ui.renderer.JsonRenderer
import com.github.kirillf.messageparser.ui.renderer.SpanRenderer

import kotlinx.android.synthetic.main.message_parser_layout.*

/**
 * Created by k.filimonov on 03/08/2017.
 */

class MessageParserActivity : AppCompatActivity(), MessageParserPresenter.View, MessageAdapter.StateClickListener {
    private var presenter: MessageParserPresenter? = null
    private lateinit var adapter: MessageAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.message_parser_layout)
        adapter = MessageAdapter(this)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        layoutManager.stackFromEnd = true
        messages.layoutManager = layoutManager
        messages.adapter = adapter
        add_message_btn.setOnClickListener {
            presenter?.onParseMessage(add_message_input.text.toString())
            add_message_input.setText("")
        }
    }

    override fun onStart() {
        super.onStart()
        attachPresenter()
    }

    private fun attachPresenter() {
        if (presenter == null) {
            presenter = lastCustomNonConfigurationInstance as MessageParserPresenter? ?: createPresenter()
        }
        presenter?.onViewAttached(this)
    }

    private fun createPresenter(): MessageParserPresenter {
        val repository = MessageRepositoryImpl()
        val messageParser = MessageParserImpl(LinkDataFetcherImpl(), UrlValidatorImpl())
        val interactor = MessagesInteractor(repository, messageParser)
        val mapper = MessageMapper(JsonRenderer(), SpanRenderer(applicationContext))
        return MessageParserPresenterImpl(interactor, mapper)
    }

    override fun onStop() {
        super.onStop()
        presenter?.onViewDetached()
    }

    override fun onRetainCustomNonConfigurationInstance(): Any {
        return presenter ?: super.onRetainCustomNonConfigurationInstance()
    }


    override fun updateMessage(position: Int) {
        adapter.notifyItemChanged(position)
    }

    override fun setMessages(messages: List<MessageViewModel>) {
        adapter.setItems(messages)
        this.messages.scrollTo(0, 0)
    }

    override fun onStateClicked(position: Int) {
        presenter?.onToggleMessage(position)
    }

    override fun addMessage(position: Int) {
        adapter.notifyItemInserted(position)
        messages.scrollToPosition(position)
    }

    override fun showError() {
        Snackbar.make(add_message_layout, R.string.default_error, Snackbar.LENGTH_SHORT).show()
    }
}
