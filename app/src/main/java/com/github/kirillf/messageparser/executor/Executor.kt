package com.github.kirillf.messageparser.executor

/**
 * Created by k.filimonov on 06/08/2017.
 */
interface Executor {
    fun execute(runnable: Runnable)
}