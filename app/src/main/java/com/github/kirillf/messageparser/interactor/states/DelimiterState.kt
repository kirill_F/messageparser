package com.github.kirillf.messageparser.interactor.states

import com.github.kirillf.messageparser.interactor.ParserStateHandler
import com.github.kirillf.messageparser.interactor.State
import com.github.kirillf.messageparser.interactor.States
import com.github.kirillf.messageparser.model.Plain

/**
 * Created by k.filimonov on 13/08/2017.
 */
class DelimiterState : State {

    override fun apply(text: String, nextSymbol: Char, messageParser: ParserStateHandler) {
        messageParser.addPlain(Plain(text))
        when {
            messageParser.isDelimiter(nextSymbol) -> messageParser.nextState(States.DELIMITER)
            messageParser.isMentionStart(nextSymbol) -> messageParser.nextState(States.MENTION)
            messageParser.isTerminal(nextSymbol) -> messageParser.nextState(States.IDLE)
            messageParser.isEmoticonStart(nextSymbol) -> messageParser.nextState(States.EMOTICON)
            messageParser.isLinkStart(nextSymbol) -> messageParser.nextState(States.LINK)
            else -> messageParser.nextState(States.PLAIN)
        }
    }
}