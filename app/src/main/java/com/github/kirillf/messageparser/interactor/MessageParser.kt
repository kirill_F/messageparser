package com.github.kirillf.messageparser.interactor

import com.github.kirillf.messageparser.model.Message

/**
 * Created by k.filimonov on 14/08/2017.
 */
interface MessageParser {
    fun parse(message: String): Message
}