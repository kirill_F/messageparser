package com.github.kirillf.messageparser.model

/**
 * Created by k.filimonov on 02/08/2017.
 */

data class Message(val tokens: List<Token>,
                   val mentions: List<Mention> = listOf(),
                   val links: List<Link> = listOf(),
                   val emoticon: List<Emoticon> = listOf())

