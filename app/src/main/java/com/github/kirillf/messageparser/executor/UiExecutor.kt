package com.github.kirillf.messageparser.executor

import android.os.Handler
import android.os.Looper

/**
 * Created by k.filimonov on 06/08/2017.
 */
class UiExecutor private constructor(): Executor {
    private object Holder {
        val INSTANCE = UiExecutor()
    }

    companion object {
        val instance: UiExecutor by lazy { Holder.INSTANCE }
    }

    val uiHandler: Handler = Handler(Looper.getMainLooper())

    override fun execute(runnable: Runnable) {
        uiHandler.post(runnable)
    }
}