package com.github.kirillf.messageparser.interactor

import com.github.kirillf.messageparser.model.Emoticon
import com.github.kirillf.messageparser.model.Link
import com.github.kirillf.messageparser.model.Mention
import com.github.kirillf.messageparser.model.Plain

interface ParserStateHandler {
    val terminal: Char
        get() = '\u0000'

    fun addPlain(plain: Plain);
    fun addMention(mention: Mention)
    fun addLink(link: Link)
    fun addEmoticon(emoticon: Emoticon)
    fun nextState(state: States)

    fun isDelimiter(symbol: Char): Boolean {
        return symbol in listOf(' ', '\n', '\r', '.', ',')
    }

    fun isTerminal(symbol: Char): Boolean {
        return symbol == terminal
    }

    fun isEmoticonStart(symbol: Char): Boolean  {
        return symbol == '('
    }

    fun isMentionStart(symbol: Char): Boolean  {
        return symbol == '@'
    }

    fun isLinkStart(symbol: Char): Boolean {
        return symbol == 'h'
    }
}