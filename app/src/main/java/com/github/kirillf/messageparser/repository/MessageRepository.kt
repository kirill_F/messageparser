package com.github.kirillf.messageparser.repository

import com.github.kirillf.messageparser.model.Message

/**
 * Created by k.filimonov on 06/08/2017.
 */
interface MessageRepository {
    fun putMessage(message: Message)
    fun getMessages(): List<Message>
}