package com.github.kirillf.messageparser.model

/**
 * Created by k.filimonov on 13/08/2017.
 */
sealed class Token
data class Plain(val text: String) : Token()
data class Mention(val name: String) : Token()
data class Link(val url: String,
                val title: String = "") : Token()
data class Emoticon(val type: String) : Token()
