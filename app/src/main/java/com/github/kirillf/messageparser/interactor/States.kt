package com.github.kirillf.messageparser.interactor

/**
 * Created by k.filimonov on 14/08/2017.
 */
enum class States {
    DELIMITER,
    EMOTICON,
    IDLE,
    LINK,
    MENTION,
    PLAIN
}