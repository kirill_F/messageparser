package com.github.kirillf.messageparser.interactor

/**
 * Created by k.filimonov on 14/08/2017.
 */
interface UrlValidator {

    fun isUrlValid(url: String): Boolean

}