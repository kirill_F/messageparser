package com.github.kirillf.messageparser.ui.model

import android.text.Spannable

/**
 * Created by k.filimonov on 06/08/2017.
 */
class MessageViewModel(var state: ViewState, val rawText: String, val renderedText: Spannable) {

    enum class ViewState {
        RAW,
        RENDERED
    }
}