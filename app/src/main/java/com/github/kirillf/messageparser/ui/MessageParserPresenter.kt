package com.github.kirillf.messageparser.ui

import com.github.kirillf.messageparser.ui.model.MessageViewModel

/**
 * Created by k.filimonov on 06/08/2017.
 */
interface MessageParserPresenter {

    interface View {
        fun setMessages(messages: List<MessageViewModel>)
        fun updateMessage(position: Int)
        fun addMessage(position: Int);
        fun showError()
    }

    fun onViewAttached(view: View)
    fun onViewDetached()
    fun onToggleMessage(position: Int)
    fun onParseMessage(message: String)
}