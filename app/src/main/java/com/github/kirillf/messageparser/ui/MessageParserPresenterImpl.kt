package com.github.kirillf.messageparser.ui

import com.github.kirillf.messageparser.executor.BackgroundExecutor
import com.github.kirillf.messageparser.executor.Executor
import com.github.kirillf.messageparser.executor.UiExecutor
import com.github.kirillf.messageparser.interactor.MessagesInteractor
import com.github.kirillf.messageparser.model.Message
import com.github.kirillf.messageparser.ui.model.MessageMapper
import com.github.kirillf.messageparser.ui.model.MessageViewModel

class MessageParserPresenterImpl(val interactor: MessagesInteractor,
                                 val messageMapper: MessageMapper,
                                 val ioExecutor: Executor = BackgroundExecutor.instance,
                                 val uiExecutor: Executor = UiExecutor.instance)
    : MessageParserPresenter, MessagesInteractor.MessagesListener {

    private var view: MessageParserPresenter.View?= null

    private val messages = mutableListOf<MessageViewModel>()

    override fun onViewAttached(view: MessageParserPresenter.View) {
        this.view = view
        if (messages.isNotEmpty()) view.setMessages(messages)
        ioExecutor.execute(Runnable { interactor.getMessages(this) })
    }

    override fun onViewDetached() {
        this.view = null
    }

    override fun onToggleMessage(position: Int) {
        if (position < 0 || position >= messages.size) {
            return
        }
        val message = messages[position]
        if (message.state == MessageViewModel.ViewState.RAW) {
            message.state = MessageViewModel.ViewState.RENDERED
        } else {
            message.state = MessageViewModel.ViewState.RAW
        }
        view?.updateMessage(position)
    }

    override fun onParseMessage(message: String) {
        ioExecutor.execute(Runnable { interactor.parse(message, this) })
    }

    override fun onMessages(messages: List<Message>) {
        val mapped = messageMapper.map(messages)
        uiExecutor.execute(Runnable {
            this.messages.clear()
            this.messages.addAll(mapped)
            view?.setMessages(this.messages)
        })
    }

    override fun onMessageParsed(message: Message) {
        val messageViewModel = messageMapper.map(message, MessageViewModel.ViewState.RAW)
        uiExecutor.execute(Runnable {
            messages.add(messageViewModel)
            view?.addMessage(messages.lastIndex)
        })
    }

    override fun onError(error: Throwable) {
        view?.showError()
    }
}