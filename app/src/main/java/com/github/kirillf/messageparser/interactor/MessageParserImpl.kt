package com.github.kirillf.messageparser.interactor

import com.github.kirillf.messageparser.interactor.states.*
import com.github.kirillf.messageparser.model.*

/**
 * Created by k.filimonov on 13/08/2017.
 */
class MessageParserImpl(val urlFetcher: LinkDataFetcher, val urlValidator: UrlValidator) : ParserStateHandler, MessageParser {

    private var buf: String = ""

    private var currentState: State = IdleState()

    private val raw = mutableListOf<Token>()
    private val plains = mutableListOf<Plain>()
    private val mentions = mutableListOf<Mention>()
    private val emoticons = mutableListOf<Emoticon>()
    private val links = mutableListOf<Link>()

    private var index = 0;

    override fun parse(message: String) : Message {
        reset()
        while(index < message.length) {
            val ch = message[index]
            currentState.apply(buf, ch, this)
            buf += ch
            index++
        }
        currentState.apply(buf, terminal, this)
        return Message(raw.toMutableList(), mentions.toList(), links.toList(), emoticons.toList())
    }

    private fun reset() {
        raw.clear()
        plains.clear()
        mentions.clear()
        emoticons.clear()
        links.clear()
        index = 0
        buf = ""
        currentState = IdleState()
    }

    override fun addPlain(plain: Plain) {
        raw.add(plain)
        plains.add(plain)
        buf = ""
    }

    override fun addMention(mention: Mention) {
        raw.add(mention)
        mentions.add(mention)
        buf = ""
    }

    override fun addLink(link: Link) {
        val title = urlFetcher.fetchTitle(link.url)
        val resolved = Link(link.url, title)
        raw.add(resolved)
        links.add(resolved)
        buf = ""
    }

    override fun addEmoticon(emoticon: Emoticon) {
        raw.add(emoticon)
        emoticons.add(emoticon)
        buf = ""
    }

    override fun nextState(state: States) {
        when(state) {
            States.DELIMITER -> currentState = DelimiterState()
            States.EMOTICON -> currentState = EmoticonState()
            States.IDLE -> currentState = IdleState()
            States.LINK -> currentState = LinksState(urlValidator)
            States.MENTION -> currentState = MentionState()
            States.PLAIN -> currentState = PlainState()
        }
    }
}
