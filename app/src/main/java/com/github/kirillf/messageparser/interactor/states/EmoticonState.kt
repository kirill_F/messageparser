package com.github.kirillf.messageparser.interactor.states

import com.github.kirillf.messageparser.interactor.ParserStateHandler
import com.github.kirillf.messageparser.interactor.State
import com.github.kirillf.messageparser.interactor.States
import com.github.kirillf.messageparser.model.Emoticon
import com.github.kirillf.messageparser.model.Plain

/**
 * Created by k.filimonov on 13/08/2017.
 */
class EmoticonState : State {

    override fun apply(text: String, nextSymbol: Char, messageParser: ParserStateHandler) {
        when {
            nextSymbol == ')' -> {
                return
            }
            messageParser.isDelimiter(nextSymbol) -> {
                handleText(text, messageParser)
                messageParser.nextState(States.DELIMITER)
            }
            nextSymbol.isLetterOrDigit() -> {
                if (isEmoticon(text)) {
                    messageParser.addEmoticon(Emoticon(text.substring(1, text.lastIndex)))
                    messageParser.nextState(States.PLAIN)
                } else if (text.length > 15) {
                    messageParser.nextState(States.PLAIN)
                }
            }
            messageParser.isTerminal(nextSymbol) -> {
                handleText(text, messageParser)
                messageParser.nextState(States.IDLE)
            }
            else -> {
                if (isEmoticon(text)) {
                    messageParser.addEmoticon(Emoticon(text.substring(1, text.lastIndex)))
                } else {
                    messageParser.nextState(States.PLAIN)
                }
            }
        }
    }

    private fun handleText(text: String, messageParser: ParserStateHandler) {
        if (isEmoticon(text)) {
            messageParser.addEmoticon(Emoticon(text.substring(1, text.lastIndex)))
        } else {
            messageParser.addPlain(Plain(text))
        }
    }

    private fun isEmoticon(text: String): Boolean {
        return text.length in 3..17 &&
                text[0] == '(' &&
               text[text.lastIndex] == ')'
    }
}