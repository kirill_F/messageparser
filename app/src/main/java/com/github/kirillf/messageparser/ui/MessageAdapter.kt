package com.github.kirillf.messageparser.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.github.kirillf.messageparser.R
import com.github.kirillf.messageparser.ui.model.MessageViewModel

/**
 * Created by k.filimonov on 10/08/2017.
 */
class MessageAdapter(val stateClickListener: StateClickListener) : RecyclerView.Adapter<MessageAdapter.MessageViewHolder>() {

    private var items: List<MessageViewModel>? = null

    override fun onBindViewHolder(holder: MessageViewHolder?, position: Int) {
        items?.get(position)?.let {
            if (it.state == MessageViewModel.ViewState.RAW) {
                holder?.messageText?.text = it.rawText
                holder?.switchText?.setText(R.string.rendered_state_text)
            } else {
                holder?.messageText?.text = it.renderedText
                holder?.switchText?.setText(R.string.raw_state_text)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MessageViewHolder {
        val itemView = LayoutInflater.from(parent!!.context).inflate(R.layout.message_item_layout, parent, false)
        return MessageViewHolder(itemView, stateClickListener)
    }

    override fun getItemCount(): Int {
        return items?.size ?: 0
    }

    fun setItems(items: List<MessageViewModel>) {
        this.items = items
        notifyDataSetChanged()
    }

    class MessageViewHolder(itemView: View, stateClickListener: StateClickListener) : RecyclerView.ViewHolder(itemView) {
        var messageText: TextView = itemView.findViewById(R.id.message_text)
        var switchText: TextView = itemView.findViewById(R.id.switch_text)

        init {
            switchText.setOnClickListener { stateClickListener.onStateClicked(adapterPosition) }
        }
    }

    interface StateClickListener {
        fun onStateClicked(position: Int)
    }
}