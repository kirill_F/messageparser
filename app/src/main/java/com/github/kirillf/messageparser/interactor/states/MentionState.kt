package com.github.kirillf.messageparser.interactor.states

import com.github.kirillf.messageparser.interactor.ParserStateHandler
import com.github.kirillf.messageparser.interactor.State
import com.github.kirillf.messageparser.interactor.States
import com.github.kirillf.messageparser.model.Mention
import com.github.kirillf.messageparser.model.Plain

/**
 * Created by k.filimonov on 13/08/2017.
 */
class MentionState : State {

    override fun apply(text: String, nextSymbol: Char, messageParser: ParserStateHandler) {
        when {
            messageParser.isDelimiter(nextSymbol) -> {
                handleText(text, messageParser)
                messageParser.nextState(States.DELIMITER)
            }
            messageParser.isTerminal(nextSymbol) -> {
                handleText(text, messageParser)
                messageParser.nextState(States.IDLE)
            }
            messageParser.isEmoticonStart(nextSymbol) -> {
                handleText(text, messageParser)
                messageParser.nextState(States.EMOTICON)
            }
            else -> {
                if (!nextSymbol.isLetterOrDigit()) {
                    messageParser.nextState(States.PLAIN)
                }
            }
        }
    }

    private fun handleText(text: String, messageParser: ParserStateHandler) {
        if (text.length > 1) {
            messageParser.addMention(Mention(text.substring(1)))
        } else {
            messageParser.addPlain(Plain(text))
        }
    }
}