package com.github.kirillf.messageparser.ui.renderer

import com.github.kirillf.messageparser.model.Message

/**
 * Created by k.filimonov on 13/08/2017.
 */
interface Renderer<out T> {
    fun render(message: Message): T
}