package com.github.kirillf.messageparser.interactor

import org.jsoup.Jsoup
import java.io.IOException
import java.net.URL

class LinkDataFetcherImpl() : LinkDataFetcher {
    private val timeout = 20 * 1000 // 20 seconds

    override fun fetchTitle(url: String): String {
        try {
            val urlObj = URL(url)
            return Jsoup.parse(urlObj, timeout).title()
        } catch (e : IOException) {
            return ""
        }
    }
}