package com.github.kirillf.messageparser.interactor.states

import com.github.kirillf.messageparser.interactor.ParserStateHandler
import com.github.kirillf.messageparser.interactor.State
import com.github.kirillf.messageparser.interactor.States
import com.github.kirillf.messageparser.interactor.UrlValidator
import com.github.kirillf.messageparser.model.Link
import com.github.kirillf.messageparser.model.Plain

/**
 * Created by k.filimonov on 14/08/2017.
 */
class LinksState(val urlValidator: UrlValidator) : State {

    // RFC 3986
    private val validUrlSymbols = listOf('-', '.', '_', '~', ':', '/', '?', '#', '[', ']', '@',
            '!', '=', '$', '&', '\'', '(', ')', '*', '+', ',', ';', '=', '`', '.')

    override fun apply(text: String, nextSymbol: Char, messageParser: ParserStateHandler) {
        when {
            nextSymbol in listOf(' ', '\n', '\t') -> {
                handleText(text, messageParser)
                messageParser.nextState(States.DELIMITER)
            }
            messageParser.isTerminal(nextSymbol) -> {
                handleText(text, messageParser)
                messageParser.nextState(States.IDLE)
            }
            !isValidSymbol(nextSymbol) -> {
                handleText(text, messageParser)
                messageParser.nextState(States.PLAIN)
            }
        }
    }

    private fun handleText(text: String, messageParser: ParserStateHandler) {
        if (urlValidator.isUrlValid(text)) {
            messageParser.addLink(Link(text))
        } else {
            messageParser.addPlain(Plain(text))
        }
    }

    private fun isValidSymbol(symbol: Char): Boolean {
        return symbol.isLetterOrDigit() ||
              symbol in validUrlSymbols
    }

}