package com.github.kirillf.messageparser.interactor

import com.github.kirillf.messageparser.model.Message
import com.github.kirillf.messageparser.repository.MessageRepository

/**
 * Created by k.filimonov on 06/08/2017.
 */
class MessagesInteractor(private val messageRepository: MessageRepository,
                         private val messageParser: MessageParser) {

    fun getMessages(listener: MessagesListener) {
        listener.onMessages(messageRepository.getMessages())
    }

    fun parse(message: String, listener: MessagesListener) {
        try {
            val parsed = messageParser.parse(message)
            messageRepository.putMessage(parsed)
            listener.onMessageParsed(parsed)
        } catch (e: Exception) {
            listener.onError(e)
        }
    }

    interface MessagesListener {
        fun onMessages(messages: List<Message>)
        fun onMessageParsed(message: Message)
        fun onError(error: Throwable)
    }

}