package com.github.kirillf.messageparser.interactor.states

import com.github.kirillf.messageparser.interactor.ParserStateHandler
import com.github.kirillf.messageparser.interactor.State
import com.github.kirillf.messageparser.interactor.States

/**
 * Created by k.filimonov on 13/08/2017.
 */
class IdleState : State {

    override fun apply(text: String, nextSymbol: Char, messageParser: ParserStateHandler) {
        when {
            messageParser.isMentionStart(nextSymbol) -> messageParser.nextState(States.MENTION)
            messageParser.isDelimiter(nextSymbol) -> messageParser.nextState(States.DELIMITER)
            messageParser.isEmoticonStart(nextSymbol) -> messageParser.nextState(States.EMOTICON)
            messageParser.isLinkStart(nextSymbol) -> messageParser.nextState(States.LINK)
            else -> messageParser.nextState(States.PLAIN)
        }
    }
}