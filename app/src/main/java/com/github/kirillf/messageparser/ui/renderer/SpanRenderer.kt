package com.github.kirillf.messageparser.ui.renderer

import android.content.Context
import android.support.v4.content.ContextCompat
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.BackgroundColorSpan
import android.text.style.URLSpan
import com.github.kirillf.messageparser.R
import com.github.kirillf.messageparser.model.*

/**
 * Created by k.filimonov on 13/08/2017.
 */
class SpanRenderer(val context: Context) : Renderer<Spannable> {

    override fun render(message: Message): Spannable {
        val builder = SpannableStringBuilder()
        message.tokens.forEach {
            val decoratedText = decorate(it)
            builder.append(decoratedText.text)
            builder.setSpan(decoratedText.span,
                    builder.lastIndex - decoratedText.text.length + 1,
                    builder.lastIndex + 1,
                    Spanned.SPAN_COMPOSING)
        }
        return builder
    }

    fun decorate(token: Token): DecoratedText = when(token) {
        is Plain -> DecoratedText(token.text, BackgroundColorSpan(ContextCompat.getColor(context, android.R.color.transparent)))
        is Mention -> DecoratedText(token.name, BackgroundColorSpan(ContextCompat.getColor(context, R.color.mentionGray)))
        is Link -> DecoratedText(getUrlText(token), URLSpan(token.url))
        is Emoticon -> DecoratedText(token.type, BackgroundColorSpan(ContextCompat.getColor(context, R.color.emoticonPrimary)))
    }

    private fun getUrlText(link: Link): String {
        return if (link.title.isBlank()) {
            link.url
        } else {
            link.title
        }
    }

    data class DecoratedText(val text: String, val span: Any)

}