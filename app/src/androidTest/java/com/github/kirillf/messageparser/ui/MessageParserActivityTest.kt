package com.github.kirillf.messageparser.ui

import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.test.espresso.Espresso.*
import android.support.test.espresso.UiController
import android.support.test.espresso.ViewAction
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.*
import android.support.test.espresso.contrib.RecyclerViewActions
import android.view.View
import android.widget.TextView
import com.github.kirillf.messageparser.R
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by k.filimonov on 16/08/2017.
 */
@RunWith(AndroidJUnit4::class)
@LargeTest
class MessageParserActivityTest {

    @Rule @JvmField
    val activityRule = ActivityTestRule(MessageParserActivity::class.java)

    @Test
    fun shouldEnterSimpleTextAndShowEmptyJsonInList() {
        performRawTest("simple text", "{}")
    }

    @Test
    fun shouldMathControlTest1() {
        performRawTest("@chris you around?", "{\"mentions\":[\"chris\"]}")
    }

    @Test
    fun shouldMathControlTest2() {
        performRawTest("Good morning! (megusta) (coffee)", "{\"emoticons\":[\"megusta\",\"coffee\"]}")
    }

    private fun performRawTest(text: String, expected: String) {
        onView(withId(R.id.add_message_input)).perform(typeText(text))
        onView(withId(R.id.add_message_btn)).perform(click())

        onView(withId(R.id.message_text)).check(matches(withText(expected)))
    }

    @Test
    fun shouldToggleCorrectViewStateOnElementInList() {
        for (i in 1..3) {
            onView(withId(R.id.add_message_input)).perform(typeText("simpe text $i"))
            onView(withId(R.id.add_message_btn)).perform(click())
        }


        onView(withId(R.id.messages)).perform(RecyclerViewActions
                .actionOnItemAtPosition<MessageAdapter.MessageViewHolder>(1, object : ViewAction {
                    override fun getDescription(): String {
                        return ""
                    }

                    override fun getConstraints(): Matcher<View> {
                        return Matchers.any(View::class.java)
                    }

                    override fun perform(p0: UiController?, p1: View?) {
                        p1?.findViewById<TextView>(R.id.switch_text)?.performClick()
                    }
                }))
    }
}